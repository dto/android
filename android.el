;;; ouya.el --- emacs tools for ouya development

;; Copyright (C) 2014  David O'Toole

;; Author: David O'Toole <dto@monad>
;; Keywords: tools

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defun ouya-boot-linux ()
  (interactive)
  (shell-command "~/ouya-dev/boot.sh"))

(defvar ouya-host "ouya")

(defun ouya-get-host (ip)
  (interactive "sEnter IP address of Ouya: ")
  (setq ouya-host ip))

(defun ouya-connect-lisp ()
  (interactive)
  (slime-connect (ouya-get-host)))


(provide 'ouya)
;;; ouya.el ends here
